public class SwipeDemo.Animation : GLib.Object {
    public signal void done ();

    public double current_value { get; private set; }

    private Gtk.Widget widget;
    private double value_from;
    private double value_to;
    private int64 duration;

    private int64 start_time;
    private uint tick_cb_id;

    public Animation (Gtk.Widget widget, double from, double to, int64 duration) {
        this.widget = widget;
        value_from = from;
        value_to = to;
        this.duration = duration;

        current_value = from;
    }

    public void stop () {
        if (tick_cb_id != 0) {
            widget.remove_tick_callback (tick_cb_id);
            tick_cb_id = 0;
        }

        done ();
    }

    public void start () {
        if (!Hdy.get_enable_animations (widget) || !widget.get_mapped () || duration <= 0) {
            current_value = value_to;

            done ();

            return;
        }

        if (tick_cb_id != 0) {
            widget.remove_tick_callback (tick_cb_id);
            tick_cb_id = 0;
        }

        start_time = widget.get_frame_clock ().get_frame_time () / 1000;
        tick_cb_id = widget.add_tick_callback (tick_cb);
    }

    private bool tick_cb (Gtk.Widget widget, Gdk.FrameClock frame_clock) {
        var frame_time = frame_clock.get_frame_time () / 1000;
        var t = (double) (frame_time - start_time) / duration;

        if (t >= 1) {
            current_value = value_to;
            tick_cb_id = 0;

            done ();

            return Source.REMOVE;
        }

        current_value = value_from + (value_to - value_from) * Hdy.ease_out_cubic (t);

        return Source.CONTINUE;
    }
}
