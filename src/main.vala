int main (string[] args) {
    var app = new Gtk.Application ("org.example.SwipeDemo", ApplicationFlags.FLAGS_NONE);

    app.startup.connect (() => {
        Hdy.init ();

        var provider = new Gtk.CssProvider ();

        provider.load_from_resource ("/org/example/SwipeDemo/style.css");

        Gtk.StyleContext.add_provider_for_screen (
            Gdk.Screen.get_default (),
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );
    });

    app.activate.connect (() => {
        var win = app.active_window ?? new SwipeDemo.Window (app);

        win.present ();
    });

    typeof (SwipeDemo.SwipeAwayBin).ensure ();

    return app.run (args);
}
