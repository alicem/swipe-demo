public class SwipeDemo.SwipeAwayBin : Gtk.EventBox, Hdy.Swipeable {
    public signal void removed ();

    private double progress;
    public Hdy.SwipeTracker tracker;
    private Animation? animation;

    construct {
        tracker = new Hdy.SwipeTracker (this);

        tracker.begin_swipe.connect ((direction, direct) => {
            if (animation != null)
                animation.stop ();
        });

        tracker.update_swipe.connect (set_progress);

        tracker.end_swipe.connect ((duration, to) => {
            animation = new Animation (this, progress, to, duration);

            animation.notify["current-value"].connect (() => {
                set_progress (animation.current_value);
            });

            animation.done.connect (() => {
                animation = null;

                if (progress.abs () >= 1)
                    Idle.add (() => {
                        removed ();

                        return Source.REMOVE;
                    });
            });

            animation.start ();
        });
    }

    private void set_progress (double progress) {
        this.progress = progress;

        opacity = 1 - progress.abs ();
        queue_allocate ();
    }

    private unowned Hdy.SwipeTracker get_swipe_tracker () {
        return tracker;
    }

    private double get_distance () {
        return (double) get_allocated_width ();
    }

    private double get_cancel_progress () {
        return 0;
    }

    private double get_progress () {
        return progress;
    }

    private double[] get_snap_points () {
        return { -1, 0, 1 };
    }

    private Gdk.Rectangle get_swipe_area (Hdy.NavigationDirection direction, bool is_drag) {
        return {
            0,
            0,
            get_allocated_width (),
            get_allocated_height (),
        };
    }

    private void switch_child (uint index, int64 duration) {
    }

    protected override void size_allocate (Gtk.Allocation alloc) {
        base.size_allocate (alloc);

        var child = get_child ();

        if (child == null || !child.visible)
            return;

        child.size_allocate ({
            alloc.x - (int) (progress * alloc.width),
            alloc.y,
            alloc.width,
            alloc.height
        });
    }
}
