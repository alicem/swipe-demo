[GtkTemplate (ui = "/org/example/SwipeDemo/window.ui")]
public class SwipeDemo.Window : Hdy.ApplicationWindow {
    [GtkChild]
    private Gtk.ListBox list;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        for (int i = 0; i < 5; i++)
            add_page ();
    }

    [GtkCallback]
    private void add_page () {
        var row = new Row ();

        row.removed.connect (() => {
            list.remove (row);
        });

        list.add (row);
        row.reveal ();
    }
}

